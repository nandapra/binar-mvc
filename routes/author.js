var express = require('express');
var router = express.Router();
const authorController = require('../controllers/authorController.js')

/* GET author page. */
router.get('/', authorController.GetAuthorData);
router.get('/:id', authorController.GetAuthorDataById);
router.post('/', authorController.PostAuthorData);
router.put('/:id', authorController.UpdateAuthorData);
router.delete('/:id', authorController.DeleteAuthorData)
module.exports = router;