var express = require('express');
var router = express.Router();
const bookController = require('../controllers/bookController.js')

/* GET book page. */
router.get('/', bookController.GetBookData);
router.get('/:id', bookController.GetBookDataById);
router.post('/', bookController.PostBookData);
router.put('/:id', bookController.UpdateBookData);
router.delete('/:id', bookController.DeleteBookData)
module.exports = router;