'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.addColumn(
      'Books',                      //nama table
      'author_id',                  //nama column baru
      {
        type: Sequelize.INTEGER,
        references: {
          model: 'Books',
          key: 'id'
        }
      }
    )
  },

  down: async (queryInterface, Sequelize) => {
    queryInterface.removeColumn('Books', 'author_id')
  }
};