const express = require('express')
const app = express()
const model = require('../models')
const book = model.Book
const author = model.Author

app.use(express.json())

exports.GetBookData = (req, res, next) => {
    book.findAll({
        order: [['id', 'ASC']],
        include: ['testAuthor']
    })
    .then(book => {
        res.status(200).json({
            success: 'true',
            data: book
        })
    })
    .catch(err => {
        res.status(422).json({
            success: 'false',
            data: err.message
        })
    })
}

exports.GetBookDataById = (req, res, next) => {
    book.findByPk(req.params.id)
    .then(book => {
        if (book == null){
            res.status(200).json({
                success: 'false',
                data: 'id tidak terdaftar'
            })
        }else{
            res.status(200).json({
                success: 'true',
                data: book
            })
        }
    })
    .catch(err => {
        res.status(422).json({
            success: 'false',
            data: err.message
        })
    })
}



exports.PostBookData = (req, res, next) => {
    author.findAndCountAll({
        where:{
            id: req.body.author_id
        }
    })
    .then((author) => {
        if( author.count != 1){
            res.status(200).json({
                success: 'false',
                message: 'Harap membuat author terlebih dahulu' 
            })
        }else{
            try{
                book.create({
                    name: req.body.name,
                    description: req.body.description,
                    publish: req.body.publish,
                    author_id: req.body.author_id
                })
                res.status(200).json({
                    success: 'true',
                    message: 'Book Created !!',
                    data: book
                })
            }catch(err){
                res.status(422).json({
                    status: 'false',
                    message: err.message
                })
            }
        }
    })
    .catch(err => {
        res.status(422).json({
            success: 'false',
            message: err.message
        })
    })
}

exports.UpdateBookData = (req, res, next) => {
    book.findByPk(req.params.id)
    .then((book) => {
        book.update ({
            name: (req.body.name == null || req.body.name == "") ? book.name : req.body.name,
            description: (req.body.description == null || req.body.description == "") ? book.description : req.body.description,
            pablish: (req.body.pablish == null || req.body.pablish == "") ? book.pablish : req.body.pablish,
            author_id: (req.body.author_id == null || req.body.author_id == "") ? book.author_id : req.body.author_id
        },{
            where : req.params.id
        }).then(() => {
            res.status(200).json({
                success: 'true',
                message: `Update data id ${req.params.id}`,
                data: book
            })
        })
    })
    .catch((book) => {
        res.status(422).json({
            success: 'false',
            message: 'id not already'
        })
    })
}

exports.DeleteBookData = (req, res, next) => {
    const idVal = req.params.id
    book.findByPk(req.params.id)
    .then((book) => {
        book.destroy({
            where: {
                id: req.params.id
            }
        })
        res.status(200).json({
            success: 'true',
            message: `row id ${req.params.id}, successfuly deleted`
        })
    })
    .catch((book) => {
        res.status(422).json({
            status: 'false',
            message: 'id not already'
        })
    })
}