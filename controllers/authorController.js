const express = require('express')
const app = express()
const model = require('../models')
const author = model.Author
const book = model.Book

app.use(express.json())

exports.GetAuthorData = (req, res, next) => {
    author.findAll({
        order: [['id', 'ASC']],
        include: ['testBook']
    })
    .then(author => {
        res.status(200).json({
            success: 'true',
            data: author
        })
    })
    .catch(err => {
        res.status(422).json({
            success: 'false',
            data: err.message
        })
    })
}

exports.GetAuthorDataById = (req, res, next) => {
    author.findAndCountAll({
        author: req.params.author_id,
        include: ['testBook']
    })
    .then(author => {
        if(author.count == 1){
            res.status(200).json({
                success: 'false',
                data: author.rows
            })
        }else{
            res.status(200).json({
                success: 'true',
                data: 'data tidak ada'
            })
        }        
    })
    .catch(err => {
        res.status(422).json({
            success: 'false',
            data: err.message
        })
    })
}

exports.PostAuthorData = (req, res, next) => {
    const emailVal = req.body.email
    author.findOrCreate({
        where:{
            email:emailVal
        },
        defaults:{
            name: req.body.name,
            email: req.body.email,
            alamat: req.body.alamat
        }
    })
    .then((author) => {
        if(author[1]){
            res.status(200).json({
                success: 'true',
                message: 'Author Created !!',
                data: author[0]
            })
        }else{
            res.status(200).json({
                success: 'false',
                message: 'Email Already',
            })
        }
    })
    .catch((err) => {
        res.status(422).json({
            success: 'false',
            message: err.message
        })
    })
}

exports.UpdateAuthorData = (req, res, next) => {
    author.findByPk(req.params.id)
    .then((author) => {
        author.update ({
            name: (req.body.name == null || req.body.name == "") ? author.name : req.body.name,
            email: (req.body.email == null || req.body.email == "") ? author.email : req.body.email,
            alamat: (req.body.alamat == null || req.body.alamat == "") ? author.alamat : req.body.alamat
        },{
            where : req.params.id
        }).then(() => {
            res.status(200).json({
                success: 'true',
                message: `Update data id ${req.params.id}`,
                data: author
            })
        })
    })
    .catch((author) => {
        res.status(422).json({
            success: 'false',
            message: 'id not already'
        })
    })
}

exports.DeleteAuthorData = (req, res, next) => {
    const idVal = req.params.id
    author.findByPk(req.params.id)
    .then((author) => {
        author.destroy({
            where: {
                id: req.params.id
            }
        })
        res.status(200).json({
            success: 'true',
            message: `row id ${req.params.id}, successfuly deleted`
        })
    })
    .catch((author) => {
        res.status(422).json({
            status: 'false',
            message: 'id not already'
        })
    })
}